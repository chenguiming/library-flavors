package com.itfitness.androidgradledemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.itfitness.mylibrary.MUtil

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val tvMsg = findViewById<TextView>(R.id.tv_msg)
        tvMsg.text = MUtil().getMsg()
    }
}